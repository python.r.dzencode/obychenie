import json
import random
import datetime

from main import timer

import logging

logging.basicConfig(filename='./log.txt', level=logging.INFO)

possible_data = [
    None,
    'asdasd',
    'asdadasd',
    'ASADasdas',
    'SADqeqadad2342342',
    'asdaf##${}[]',
    'asdt54453sffs   ',
    ' ',
    '1.2',
    random.randint(1, 999999),
    random.randint(1, 999999),
    random.randint(1, 999999),
    random.randint(1, 999999),
    random.randint(1, 999999),
    random.randint(1, 999999),
    random.randint(1, 999999),
    random.randint(1, 999999),
    f'{round(random.random(), 2)}',
    f'{round(random.random(), 2)}',
    f'{round(random.random(), 2)}',
    f'{round(random.random(), 2)}',
    f'{round(random.random(), 2)}',
    f'{round(random.random(), 2)}',
    f'{round(random.random(), 2)}',
    f'{round(random.random(), 2)}',
    [],
    ['asdasd' for _ in range(1, 10)],
    ['123123' for _ in range(1, 10)],
    ['53434.523' for _ in range(1, 10)],
    [],
    [],
    {'hello': 'world'},
    {'hello': 'world'},
    {},
]


@timer
def set_data():
    msg = f'{datetime.datetime.now()} - Generated data: Func "set_data"'
    logging.info(msg)
    return [random.choice(possible_data) for _ in range(1, 99999)]


@timer
def set_data_to_json():
    with open('data/set_data.json', 'w', encoding='utf-8') as file:
        data = {'data': set_data()}
        json.dump(data, file, ensure_ascii=False)

    msg = f'{datetime.datetime.now()} - Write data in JSON set_data.json. Func: "set_data_to_json"'
    logging.info(msg)


set_data_to_json()
