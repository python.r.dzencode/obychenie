import json
import datetime
import logging
from multiprocessing import Process, Queue

logging.basicConfig(filename='./log.txt', level=logging.INFO)


def timer(func, *args, **kwargs):

	def timer_wrapper():
		start = datetime.datetime.now()
		res = func(*args, **kwargs)
		stop = datetime.datetime.now() - start
		msg = f'{datetime.datetime.now()} - time for {func.__name__}: {stop}'
		logging.info(msg)
		print('time:', stop, f'func name: "{func.__name__}"')
		return res
	return timer_wrapper


@timer
def get_data():
	logging.info('-----------------------')
	msg = f'{datetime.datetime.now()} - got data from JSON set_data.json. Func: "get_data"'
	logging.info(msg)
	with open('data/set_data.json', encoding='utf-8') as file:
		data_ = json.load(file)['data']

	return data_


def set_result(result_: int):
	msg = f'{datetime.datetime.now()} - set up result data to JSON result.json. Func: "set_result"'
	logging.info(msg)
	with open('data/result.json', 'w', encoding='utf-8') as file:
		d = {'result': result_}
		json.dump(d, file, indent=4, ensure_ascii=False)


def list_sum(array_: list):
	sum_ = 0

	for el in array_:
		if str(el).isnumeric() or str(el).isdigit():
			sum_ += int(el)

		try:
			el = float(str(el))
		except ValueError:
			continue
		else:

			sum_ += float(str(el))

	msg = f'{datetime.datetime.now()} - Calculated the amount of data. Summa: {sum_}. Func: "list_sum"'
	logging.info(msg)

	return sum_


def sum_process(q_: Queue):

	if not q_.empty():
		msg = f'{datetime.datetime.now()} - Queue is not empty. We start the process. Func: "sum_process"'
		logging.info(msg)
		data_ = q_.get()
		msg = f'{datetime.datetime.now()} - got task from queue. Func: "sum_process"'
		logging.info(msg)
		result = list_sum(data_)
		set_result(result)

	msg = f'{datetime.datetime.now()} - Queue is empty. We stop the process. Func: "sum_process"'
	logging.info(msg)


if __name__ == '__main__':

	data = get_data()
	q = Queue()

	q.put(data)

	process = Process(target=sum_process, name=f'{1}', args=(q, ))
	msg = f'{datetime.datetime.now()} - started the process. Process name: {process.name}. Func: "None"'
	logging.info(msg)
	process.start()
	process.join()

	msg = f'{datetime.datetime.now()} - program is done. Func: "None"'
	logging.info(msg)
	msg = '-----------------------'
	logging.info(msg)
